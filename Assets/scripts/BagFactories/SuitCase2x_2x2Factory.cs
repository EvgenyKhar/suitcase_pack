﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuitCase2x_2x2Factory : BagFactory
{
    public override GameObject GetBagObject()
    {
        return bagObject;
    }

    public override IPackable InstantiateBag()
    {
        bagsParent = GameObject.Find("Bags");
        bagObject = Instantiate(bagPrefab, bagsParent.transform.position, Quaternion.identity, bagsParent.transform);
        bagPackable = bagObject.GetComponent<IPackable>();
        return bagPackable;
    }

    
}
