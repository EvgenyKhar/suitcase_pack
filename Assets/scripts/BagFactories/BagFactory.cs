﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BagFactory : MonoBehaviour
{
    [SerializeField] protected GameObject bagPrefab;
    protected GameObject bagsParent;
    protected IPackable bagPackable;
    protected GameObject bagObject;
    public abstract IPackable InstantiateBag();
    public abstract GameObject GetBagObject();
}
