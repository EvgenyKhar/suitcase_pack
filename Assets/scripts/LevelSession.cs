﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelSession : MonoBehaviour
{
    [SerializeField] GameObject[] bagFactories;
    [SerializeField] GameObject[] goodsFactories;
    IPackable bag;
    BagFactory bagFactory;
    [SerializeField] int levelNumber=1;
    int prePackedCount = 2;
    int preUnpackedCount = 2;
    [SerializeField] GameObject movableObjectsParent;
    [SerializeField] GameObject successImage, unSuccessImage;
    
   

    void Start()
    {
        InitializeLevel();
    }

    public void InitializeLevel()
    {
        try{InstantiateBag();}
        catch { Debug.LogError("can't instantiate bag"); }


        try { InstantiateUnpackedGoods(); }
        catch { Debug.LogError("can't instantiate unpacked goods"); }



        System.Random r = new System.Random();
        int m = r.Next();
        for (int i = 0; i < prePackedCount; i++)
        {
            print("instantiate goods in bag");
        }
    }

    private void InstantiateUnpackedGoods()
    {
        foreach (GameObject goodsFactory in goodsFactories)
        {
            if(goodsFactory.GetComponent<Goods1x1Factory>()!=null)
            {
                GoodsFactory factory = Instantiate(goodsFactory, transform).GetComponent<GoodsFactory>();
                factory.InstantiateGoods(isPacked: false, count: preUnpackedCount);
            }
        }
    }

    private void InstantiateBag()
    {
        foreach (GameObject bagFactory in bagFactories)
        {
            if(bagFactory.GetComponent<SuitCase2x_2x2Factory>()!=null)
            {
                bag = Instantiate(bagFactory, transform).GetComponent<BagFactory>().InstantiateBag();
            }
        }   
    }

    public void TryPackBag()
    {

        if(movableObjectsParent.GetComponentInChildren<Chest>() == null)
        {
            print("all objects packed");
        }


        Cell mistakenCell = bag.TryPack();
        Transform bagTransform = bag.GetTransform();
        if (mistakenCell==null)
        {
            Instantiate(successImage, bagTransform.position, Quaternion.identity, bagTransform);
        }
        else
        {
            GameObject crossImage = Instantiate(unSuccessImage, mistakenCell.transform.position, Quaternion.identity, mistakenCell.transform);
            Destroy(crossImage, 1f);
        }
    }

}
