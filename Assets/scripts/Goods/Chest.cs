﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Chest : Goods, IPuttable, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    Transform defaultParent;
    Transform oldTransform;
    Vector3 oldPosition;
    IPlaceable suitcase;
    Camera mainCam;
    List<Cell> containedCells;

    void Start()
    {
        suitcase = FindObjectOfType<BagFactory>().GetBagObject().GetComponent<IPlaceable>();
        containedCells = new List<Cell>();
        defaultParent = GameObject.Find("MovableObjects").transform;
        mainCam = Camera.main;
        Physics.queriesHitTriggers = true;
    }
    protected override void FillCellOrigins()
    {
        goodsCellOrigin = new List<Vector3>();
        goodsCellOrigin.Add(transform.position);
    }

    public void PutTo(List<Cell> tryPutCells, IPlaceable to)
    {
        Vector3 newPosition = to.PlaceIn(this, tryPutCells);
        if(newPosition != Vector3.zero)
        {
            try
            { oldTransform.GetComponent<IPlaceable>().Unplace(containedCells); }
            catch
            { Debug.Log($"{oldTransform.gameObject.name} has no IPlaceable"); }

            containedCells = tryPutCells;
            transform.SetParent(to.GetGameObject().transform);
            transform.position = newPosition;
        }
        else
        {
            ReturnToOldPlace();
            Debug.Log($"Can't place in {to}");
        }
    }

    private void ReturnToOldPlace()
    {
        transform.SetParent(oldTransform);
        transform.position = oldPosition;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        oldPosition = transform.position;
        oldTransform = transform.parent;
        transform.SetParent(defaultParent);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = mainCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        List<Cell> tryPutCells = TryPutCell();
        if (tryPutCells!=null)
        {
            PutTo(tryPutCells, suitcase);
        }
    }

    public List<Cell> TryPutCell()
    {
        FillCellOrigins();
        List<Cell> hits = new List<Cell>();
        foreach (Vector3 origin in goodsCellOrigin)
        {
            RaycastHit hit;
            if (Physics.Raycast(origin,Vector3.forward, out hit, 1000f))
            {
                Cell hitedCell = hit.transform.gameObject.GetComponent<Cell>();
                if (hitedCell != null)
                {
                    hits.Add(hitedCell);
                }
                else {return null;}
            }
            else { return null; }
        }
        return hits;
    }


}
