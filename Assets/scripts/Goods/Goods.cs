﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Goods : MonoBehaviour
{
    public Vector2 dimension;
    public List<Vector3> goodsCellOrigin;
    protected abstract void FillCellOrigins();
}
