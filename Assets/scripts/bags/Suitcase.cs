﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Suitcase : MonoBehaviour, IPlaceable, IPackable
{
    List<Inventory> inventories;

    void Start()
    {
        inventories = FindObjectsOfType<Inventory>().ToList();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public Vector3 PlaceIn(Goods goods, List<Cell> cells)
    {
        foreach (Inventory inventory in inventories)
        {
            if(inventory.IsContaisCells(cells))
            {
                return inventory.PlaceIn(goods, cells);
            }
        }
        return Vector3.zero;
    }

    public bool Unplace(List<Cell> cells)
    {
        foreach (Inventory inventory in inventories)
        {
            if (inventory.IsContaisCells(cells))
            {
                return inventory.Unplace(cells);
            }
        }
        return false;
    }

    public Cell TryPack()
    {
        for (int i = 0; i < inventories[0].inventoryCells.Count; i++)
        {
            if(i%2==0)
            {
                if(inventories[0].inventoryCells[i].isBusy && inventories[1].inventoryCells[i+1].isBusy)
                {
                    return inventories[0].inventoryCells[i];
                }
            }
            else
            {
                if (inventories[0].inventoryCells[i].isBusy && inventories[1].inventoryCells[i - 1].isBusy)
                {
                    return inventories[0].inventoryCells[i];
                }
            }
        }

        return null;
    }

    public Transform GetTransform()
    {
        return transform;
    }
}
