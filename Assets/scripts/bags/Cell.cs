﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public bool isBusy { get; private set; } = false;

    public bool PutIn(Goods goods)
    {
        if (!isBusy)
        {
            isBusy = true;
            return true;
        }
        return false;
    }

    public void Release()
    {
        isBusy = false;
    }
}
