﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPackable 
{
    Cell TryPack();
    Transform GetTransform();
}
