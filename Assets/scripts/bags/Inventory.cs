﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Cell> inventoryCells { get; private set; }
    

    void Start()
    {
        inventoryCells = GetComponentsInChildren<Cell>().ToList();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public Vector3 PlaceIn(Goods goods, List<Cell> cells)
    {
        Vector3 resultPosition = Vector3.zero;
        foreach (Cell cell in cells)
        {
            if(!cell.PutIn(goods))
            {
                return Vector3.zero;
            }
            else
            {
                resultPosition += cell.transform.position;
            }
        }
        
        return resultPosition/cells.Count;
    }

    public bool Unplace(List<Cell> cells)
    {
        foreach (Cell cell in cells)
        {
            cell.Release();
        }
        return false;
    }

    public bool IsContaisCells(List<Cell> cells)
    {
        foreach (Cell cell in cells)
        {
            if(!inventoryCells.Contains(cell))
            {
                return false;
            }
        }
        return true;
    }
}
