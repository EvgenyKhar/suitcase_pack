﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlaceable
{
    Vector3 PlaceIn(Goods goods, List<Cell> cells);
    GameObject GetGameObject();
    bool Unplace(List<Cell> cells);
}
