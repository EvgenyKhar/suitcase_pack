﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GoodsFactory : MonoBehaviour
{
    [SerializeField] public GameObject goodsPrefab;
    protected Transform goodsParent;
    protected float goodsOffset;
    public abstract void InstantiateGoods(bool isPacked, int count);
}
