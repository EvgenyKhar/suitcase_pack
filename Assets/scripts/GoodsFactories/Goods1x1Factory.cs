﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goods1x1Factory : GoodsFactory
{

    void Start()
    {
        goodsOffset = 0.2f;
    }

    public override void InstantiateGoods(bool isPacked, int count)
    {
        
        if(!isPacked)
        {
            for (int i = 0; i < count; i++)
            {
                Vector3 goodsPos = transform.TransformPoint(new Vector3(Mathf.Pow(-1f, i) * (goodsPrefab.transform.localScale.x + goodsOffset) + goodsParent.localPosition.x,
                    goodsParent.localPosition.y));
                Instantiate(goodsPrefab, goodsPos, Quaternion.identity, goodsParent);
            }
        }
        else
        {

        }

    }

}
